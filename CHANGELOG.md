# Change Log
All enhancements and patches to cookiecutter-mayan will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [2016-01-31]
### Added
- Forked from cookiecutter-djangopackage (0268e7e97e4c575ee19e8802df75877d8e21f9d5)
