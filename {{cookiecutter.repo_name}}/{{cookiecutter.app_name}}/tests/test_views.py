from __future__ import unicode_literals

from django.core.files.base import File
from django.test import override_settings

from common.tests.test_views import GenericViewTestCase
from documents.models import DocumentType, Document
from documents.tests import TEST_DOCUMENT_TYPE
from user_management.tests.literals import (
    TEST_USER_PASSWORD, TEST_USER_USERNAME
)

from ..literals import DEFAULT_EXTRA_DATA
from ..permissions import permission_extra_data_view

from .literals import TEST_SMALL_DOCUMENT_PATH


# To speed up tests, turn off global OCR processing.
@override_settings(OCR_AUTO_OCR=False)
class ExtraDataViewsTestCase(GenericViewTestCase):
    # The GenericViewTestCase creates an user, an admin user
    # a test group, test role, and invalides the permission cache
    # These test instances are stored as test case instance attibutes:
    # - self.admin_user
    # - self.user
    # - self.group
    # - self.role
    
    def setUp(self):
        super(ExtraDataViewsTestCase, self).setUp()
        self.document_type = DocumentType.objects.create(
            label=TEST_DOCUMENT_TYPE
        )
        
        with open(TEST_SMALL_DOCUMENT_PATH) as file_object:
            self.document = self.document_type.new_document(
                file_object=File(file_object), _user=self.user
            )
            
    def tearDown(self):
        super(ExtraDataViewsTestCase, self).tearDown()
        self.document_type.delete()
    
    # Test each view at least twice with the required permission and without
    def test_documet_extra_data_view_no_permission(self):
        self.login(
            username=TEST_USER_USERNAME, password=TEST_USER_PASSWORD
        )
        
        response = self.get(
            '{{ cookiecutter.app_name }}:document_extra_data_view',
            args=(self.document.pk,)
        )
        
        self.assertEqual(response.status_code, 403)

    def test_documet_extra_data_view_permission(self):
        self.login(
            username=TEST_USER_USERNAME, password=TEST_USER_PASSWORD
        )

        self.role.permissions.add(
            permission_extra_data_view.stored_permission
        )

        response = self.get(
            '{{ cookiecutter.app_name }}:document_extra_data_view',
            args=(self.document.pk,)
        )
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            self.document.extra_data.data, DEFAULT_EXTRA_DATA
        )
